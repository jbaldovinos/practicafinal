package deloitte.academy.lesson6.run;

/**
 * @author jbaldovinos@externosdeloittemx.com
 * @version 2.0
 * @since 11/03/2020
 * {@docRoot https://www.tutorialspoint.com/java/java_documentation.htm}
 */
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson6.enums.Codigos;
import deloitte.academy.lesson6.machine.Maquina;

/**
 * Clase que se encarga de traer todos los metodos creados dentro de la clase
 * Maquina
 * 
 *
 */
public class Run {

	private static final Logger LOGGER = Logger.getLogger(Run.class.getName());
	public static final ArrayList<Maquina> contenido = new ArrayList<Maquina>();

	public static void main(String[] args) {

		/**
		 * Se generan los objetos que se encuentran disponibles dentro de la maquina
		 * dispensadora
		 * 
		 */
		Maquina lista = new Maquina();
		lista.setCodigo("A1");
		lista.setArticulo(Codigos.A1.getCodigos());
		lista.setPrecio(10.5);
		lista.setCantidad(10);
		lista.setCodigoQR(1);

		Maquina lista1 = new Maquina();
		lista1.setCodigo("A2");
		lista1.setArticulo(Codigos.A2.getCodigos());
		lista1.setPrecio(15.5);
		lista1.setCantidad(4);
		lista1.setCodigoQR(2);

		Maquina lista2 = new Maquina();
		lista2.setCodigo("A3");
		lista2.setArticulo(Codigos.A3.getCodigos());
		lista2.setPrecio(22.5);
		lista2.setCantidad(2);
		lista2.setCodigoQR(3);

		Maquina lista3 = new Maquina();
		lista3.setCodigo("A4");
		lista3.setArticulo(Codigos.A4.getCodigos());
		lista3.setPrecio(8.75);
		lista3.setCantidad(6);
		lista3.setCodigoQR(4);

		Maquina lista4 = new Maquina();
		lista4.setCodigo("A5");
		lista4.setArticulo(Codigos.A5.getCodigos());
		lista4.setPrecio(30);
		lista4.setCantidad(10);
		lista4.setCodigoQR(5);

		Maquina lista5 = new Maquina();
		lista5.setCodigo("A6");
		lista5.setArticulo(Codigos.A6.getCodigos());
		lista5.setPrecio(15);
		lista5.setCantidad(2);
		lista5.setCodigoQR(6);

		Maquina lista6 = new Maquina();
		lista6.setCodigo("B1");
		lista6.setArticulo(Codigos.B1.getCodigos());
		lista6.setPrecio(10);
		lista6.setCantidad(3);
		lista6.setCodigoQR(7);

		Maquina lista7 = new Maquina();
		lista7.setCodigo("B2");
		lista7.setArticulo(Codigos.B2.getCodigos());
		lista7.setPrecio(120);
		lista7.setCantidad(6);
		lista7.setCodigoQR(8);

		Maquina lista8 = new Maquina();
		lista8.setCodigo("B3");
		lista8.setArticulo(Codigos.B3.getCodigos());
		lista8.setPrecio(10.10);
		lista8.setCantidad(10);
		lista8.setCodigoQR(9);

		Maquina lista9 = new Maquina();
		lista9.setCodigo("B4");
		lista9.setArticulo(Codigos.B4.getCodigos());
		lista9.setPrecio(3.14);
		lista9.setCantidad(10);
		lista9.setCodigoQR(10);

		Maquina lista10 = new Maquina();
		lista10.setCodigo("B5");
		lista10.setArticulo(Codigos.B5.getCodigos());
		lista10.setPrecio(15.55);
		lista10.setCantidad(0);
		lista10.setCodigoQR(11);

		Maquina lista11 = new Maquina();
		lista11.setCodigo("B6");
		lista11.setArticulo(Codigos.B6.getCodigos());
		lista11.setPrecio(12.25);
		lista11.setCantidad(4);
		lista11.setCodigoQR(12);

		Maquina lista12 = new Maquina();
		lista12.setCodigo("C1");
		lista12.setArticulo(Codigos.C1.getCodigos());
		lista12.setPrecio(10);
		lista12.setCantidad(1);
		lista12.setCodigoQR(13);

		Maquina lista13 = new Maquina();
		lista13.setCodigo("C2");
		lista13.setArticulo(Codigos.C2.getCodigos());
		lista13.setPrecio(14.75);
		lista13.setCantidad(6);
		lista13.setCodigoQR(14);

		Maquina lista14 = new Maquina();
		lista14.setCodigo("C3");
		lista14.setArticulo(Codigos.C3.getCodigos());
		lista14.setPrecio(13.15);
		lista14.setCantidad(10);
		lista14.setCodigoQR(15);

		Maquina lista15 = new Maquina();
		lista15.setCodigo("C4");
		lista15.setArticulo(Codigos.C4.getCodigos());
		lista15.setPrecio(22);
		lista15.setCantidad(9);
		lista15.setCodigoQR(16);

		ArrayList<Maquina> contenido = new ArrayList<Maquina>();

		/**
		 * Se crea un ArrayList con los objetos que se programaron previamente
		 */
		contenido.add(lista);
		contenido.add(lista1);
		contenido.add(lista2);
		contenido.add(lista3);
		contenido.add(lista4);
		contenido.add(lista5);
		contenido.add(lista6);
		contenido.add(lista7);
		contenido.add(lista8);
		contenido.add(lista9);
		contenido.add(lista10);
		contenido.add(lista11);
		contenido.add(lista12);
		contenido.add(lista13);
		contenido.add(lista14);
		contenido.add(lista15);
		for (Maquina aux : contenido) {
			System.out.println(aux.toString());
		}

		/**
		 * Se genera el nuevo producto que se agrega a el contenido de la maquina
		 * dispensadora
		 * 
		 */
		Maquina nuevoProducto = new Maquina();
		nuevoProducto.setCodigo("C5");
		nuevoProducto.setArticulo(Codigos.C5.getCodigos());
		nuevoProducto.setPrecio(12);
		nuevoProducto.setCantidad(15);
		nuevoProducto.setCodigoQR(17);

		/**
		 * Se llama el metodo para agregar un nuevo producto
		 * 
		 * @exception "Exception occur"
		 */

		try {

			Maquina.agregar(nuevoProducto);
			LOGGER.info("El nuevo producto agregado es: " + Codigos.C5.getCodigos() + "       Codigo:"
					+ Run.contenido.get(0).getCodigo() + "       Cantidad:" + Run.contenido.get(0).getCantidad());
			LOGGER.info(Run.contenido.get(0).getCodigo());

		}

		catch (ArrayIndexOutOfBoundsException ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);

		}
		for (Maquina aux : contenido) {
			System.out.println(aux.toString());
		}

		/**
		 * El metodo elimina poro completo el producto seleccionado
		 * 
		 * @see "Exception occur"
		 */

		try {

			Maquina.remover(contenido, nuevoProducto);
			LOGGER.info("El producto eliminado es: " + Codigos.B5.getCodigos());

		} catch (ArrayIndexOutOfBoundsException ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);

		}

		/**
		 * El metodo genera una venta y actualiza la cantidad total del producto
		 * seleccionado
		 * 
		 * @exception "Exception occur"
		 */
		try {

			Maquina.venta(contenido, nuevoProducto);
			System.out.print("Selecciona un articulo");
			LOGGER.info("Has comprado: " + Codigos.B3.getCodigos());

		} catch (ArrayIndexOutOfBoundsException ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);

		}

		/**
		 * Se genera otra venta y actualiza la cantidad que existe del producto
		 * seleccionado
		 * 
		 * @throws "Exception occur"
		 * 
		 */
		try {

			Maquina.venta1(contenido, nuevoProducto);
			System.out.print("Selecciona un articulo");
			LOGGER.info("Has comprado: " + Codigos.A3.getCodigos());

		} catch (ArrayIndexOutOfBoundsException ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);

		}

		/**
		 * Se genera una venta y se actualiza la cantidad total del producto en caso de
		 * que tenga
		 * 
		 * @throws "Exception occur"
		 * 
		 */
		try {

			Maquina.venta2(contenido, nuevoProducto);

			LOGGER.info("El producto seleccionado esta agotado" + Codigos.B5.getCodigos());

		} catch (ArrayIndexOutOfBoundsException ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);

		}

		try {

			Maquina.vendidos(1);

			LOGGER.info("Articulos vendidos " + "           " + "2");

		} catch (ArrayIndexOutOfBoundsException ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);

		}
	}
}
