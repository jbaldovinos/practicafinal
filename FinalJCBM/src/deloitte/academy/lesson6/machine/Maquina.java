package deloitte.academy.lesson6.machine;

/**
 * El porgrama se encarga de hacer la simulacion de una maquina dispensadora
 * 
 * @author jbaldovinos@externosdeloitte.mx.com
 * @version 2.0
 * @since 11/03/2020
 * @link https://www.tutorialspoint.com/java/java_documentation.htm
 */

import java.util.ArrayList;
import deloitte.academy.lesson6.run.Run;

/**
 * Se genera la clase que contiene las operaciones logicas del programa
 *
 */
public class Maquina {

	/**
	 * Declaracion de variables que se usan dentro de la maquina dispensadora
	 */
	static int resta4 = 2;

	public String codigo;
	public String articulo;
	public double precio;
	public int cantidad;
	public int codigoQR;

	public Maquina() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor Principal
	 * 
	 * @param codigo   String
	 * @param articulo String
	 * @param precio   double
	 * @param cantidad int
	 * @param codigoQR int
	 */
	public Maquina(String codigo, String articulo, double precio, int cantidad, int codigoQR) {
		super();
		this.codigo = codigo;
		this.articulo = articulo;
		this.precio = precio;
		this.cantidad = cantidad;
		this.codigoQR = codigoQR;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getCodigoQR() {
		return codigoQR;
	}

	public void setCodigoQR(int codigoQR) {
		this.codigoQR = codigoQR;
	}

	/**
	 * Metodo que se usa para agregar un nuevo producto dentro de la maquina
	 * dispensadora
	 * 
	 * @param nuevoProducto = Es el producto nuevo agregado a la maquina
	 *                      dispensadora
	 * 
	 */
	public static void agregar(Maquina nuevoProducto) {
		Run.contenido.add(nuevoProducto);
	}

	public Maquina(Maquina m) {
		super();
		this.codigo = m.codigo;
		this.articulo = m.articulo;
		this.precio = m.precio;
		this.cantidad = m.cantidad;
		this.codigoQR = m.codigoQR;
	}

	/**
	 * Override que se ocupa para generar el string de los objetos que se encuentran
	 * dentro del ArrayList principal
	 * 
	 * {@code public String toString() {
		return "Maquina [codigo=" + codigo + ", articulo=" + articulo + ", precio=" + precio + ", cantidad=" + cantidad
	 * + ", codigoQR=" + codigoQR + "]"; }}
	 * 
	 */

	@Override
	public String toString() {
		return "Maquina [codigo=" + codigo + ", articulo=" + articulo + ", precio=" + precio + ", cantidad=" + cantidad
				+ ", codigoQR=" + codigoQR + "]";
	}

	/**
	 * Metodo que quita un producto que se encuentra dentro de la maquina
	 * dispensadora
	 * 
	 * @param productos : Producto que se desea eliminar de la maquina dispensadora
	 * @param quitar    : Producto que se quiere eliminar
	 * @return : Lista actualizada de los productos disponibles
	 */
	public static ArrayList<Maquina> remover(ArrayList<Maquina> productos, Maquina quitar) {
		ArrayList<Maquina> actualizacion = new ArrayList<Maquina>();

		actualizacion = productos;

		for (Maquina elemento : productos) {
			if (elemento.getArticulo().equals(quitar.getArticulo())) {
				Run.contenido.remove(quitar);

			}
		}
		return actualizacion;
	}

	/**
	 * Metodo que se usa para generar una venta y restar la venta al inventario del
	 * producto Funcion que se utiliza para restar el producto del inventario
	 * {@code resta = (Run.contenido.get(9).getCantidad()) - 1}
	 * 
	 * @param productos : Producto seleccionado para la venta
	 * @param quitar    :Producto que se quiere vender y eliminar de la cantidad
	 *                  total
	 * @return : Cantidad actualizada despues de la venta
	 * 
	 * 
	 */
	public static int venta(ArrayList<Maquina> productos, Maquina quitar) {
		int resta = 1;
		for (Maquina elemento : productos) {
			if (elemento.getArticulo().equals(quitar.getArticulo())) {
				resta = (Run.contenido.get(9).getCantidad()) - 1;

			}

		}

		return resta;

	}

	/**
	 * Metodo que simula la segunda venta de la maquina dispensadora Funcion que se
	 * utiliza para restar el producto del inventario
	 * {@code resta = (Run.contenido.get(9).getCantidad()) - 1}
	 * 
	 * @param productos : Producto seleccionado para la venta
	 * @param quitar    : Producto que se quiere vender y eliminar 1 de la cantidad
	 *                  total
	 * @return : Cantidad actualizada despues de la venta
	 * 
	 */
	public static int venta1(ArrayList<Maquina> productos, Maquina quitar) {

		int resta2 = 1;

		for (Maquina elemento : productos) {
			if (elemento.getArticulo().equals(quitar.getArticulo())) {
				resta2 = (Run.contenido.get(12).getCantidad()) - 1;

			}

		}
		return resta2;
	}

	/**
	 * Metodo que simula la tercera venta de la maquina dispensadora
	 * 
	 * 
	 * @param productos : Producto seleccionado para la venta
	 * @param quitar    : Elimina uno de la cantidad total del producto seleccionado
	 * 
	 * @return : Cantidad actualizada despues de la venta
	 * 
	 */

	public static int venta2(ArrayList<Maquina> productos, Maquina quitar) {

		int resta = 0;

		for (Maquina elemento : productos) {
			if (elemento.getArticulo().equals(quitar.getArticulo())) {
				resta = (Run.contenido.get(11).getCantidad()) - 1;

			}

		}
		return resta;
	}

	public static int vendidos(int resta2) {
		return resta2 = 2;
	}
}