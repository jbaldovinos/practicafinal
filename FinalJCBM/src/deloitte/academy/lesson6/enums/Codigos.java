package deloitte.academy.lesson6.enums;

/**
 * La clase de enums, valores fijos que se tendan dentro de la maquina
 * dispensadora
 * 
 *
 */
public enum Codigos {
	A1("Chocolate"), A2("Doritos"), A3("Coca"), A4("Gomitas"), A5("Chips"), A6("Jugo"), B1("Galletas"), B2("Canelitas"),
	B3("Halls"), B4("Tarta"), B5("Sabritas"), B6("Cheetos"), C1("Rocaleta"), C2("Rancheritos"), C3("Ruffles"),
	C4("Pizza Fr�a"), C5("Sabritones");

	public String codigos;

	public String getCodigos() {
		return codigos;
	}

	public void setCodigos(String codigos) {
		this.codigos = codigos;
	}

	private Codigos(String codigos) {
		this.codigos = codigos;
	}

}
